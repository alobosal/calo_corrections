import numpy as np
import matplotlib.pyplot as plt
import pylab as pl
from scipy.integrate import quad, dblquad
import os

# Erec/Etot simulated
def get_E(rx,ry,Rm,x0,x1,y0,y1):
    integ=lambda y, x: 1/(2*np.pi*Rm**2) * np.exp( -(x-rx)**2/(2*Rm**2) -(y-ry)**2/(2*Rm**2))
    return dblquad(integ, x0,x1,lambda x:y0, lambda x:y1)[0]

def get_E_cells(rx,ry,Rm,a):
    E_cells=np.zeros((3,3))
    cells_borders=np.zeros((3,3),tuple)
    cells_centers=np.zeros((3,3),tuple)
    for i in range(3):
        for j in range(3):
            ix=i-1
            jy=j-1
            cells_borders[i,j]=(-a/2+ix*a,a/2+ix*a,-a/2+jy*a,a/2+jy*a)
            cells_centers[i,j]=(ix*a,jy*a)
            E_cells[i,j]=get_E(rx,ry,Rm,*cells_borders[i,j])
    return E_cells, cells_centers, cells_borders

def E_method(method, E_cells, rx, ry):
    if (method == "2x2"):
        if   (rx<=0 and ry<=0):
            E=E_cells[0,0]+E_cells[1,0]+E_cells[0,1]+E_cells[1,1]
        elif (rx>=0 and ry<=0):
            E=E_cells[1,0]+E_cells[2,0]+E_cells[1,1]+E_cells[2,1]
        elif (rx<=0 and ry>=0):
            E=E_cells[0,1]+E_cells[1,1]+E_cells[0,2]+E_cells[1,2]
        elif (rx>=0 and ry>=0):
            E=E_cells[1,1]+E_cells[2,1]+E_cells[1,2]+E_cells[2,2]
        else:
            E=0
    if (method == "3x3"):
        E = E_cells.sum()
    if (method == "swisscross"):
        E = E_cells[1,0]+E_cells[0,1]+E_cells[1,1]+E_cells[2,1]+E_cells[1,2]
    return E

def get_params_method(method, E_cells, cells_centers, rx, ry):
    if (method == "3x3"):
        cells_indexes= [(i,j) for i in range(3) for j in range(3)]
    if (method == "swisscross"):
        cells_indexes = [(1,0),(0,1),(1,1),(2,1),(1,2)]
    if (method == "2x2"):
        if   (rx<=0 and ry<=0):
            cells_indexes=((0,0),(1,0),(0,1),(1,1))
        elif (rx>=0 and ry<=0):
            cells_indexes=((1,0),(2,0),(1,1),(2,1))
        elif (rx<=0 and ry>=0):
            cells_indexes=((0,1),(1,1),(0,2),(1,2))
        elif (rx>=0 and ry>=0):
            cells_indexes=((1,1),(2,1),(1,2),(2,2))
    E=0
    r_reco_x=0
    r_reco_y=0
    for (i,j) in cells_indexes:
        r_reco_x+=E_cells[i,j]*cells_centers[i,j][0]
        r_reco_y+=E_cells[i,j]*cells_centers[i,j][1]
        E+=E_cells[i,j]
    r_reco_x=r_reco_x/E
    r_reco_y=r_reco_y/E
    r_reco_vec=(r_reco_x,r_reco_y)
    r_reco=np.sqrt((r_reco_x)**2+(r_reco_y)**2)
    d_true_reco=np.sqrt((r_reco_x-rx)**2+(r_reco_y-ry)**2)
    if (method == "2x2" and False):
        if   (rx<=0 and ry<=0):
            new_r0=(-a/2,-a/2)
        elif (rx>=0 and ry<=0):
            new_r0=(a/2,-a/2)
        elif (rx<=0 and ry>=0):
            new_r0=(-a/2,a/2)
        elif (rx>=0 and ry>=0):
            new_r0=(a/2,a/2)
        r_reco=np.sqrt((new_r0[0]-rx)**2+(new_r0[1]-ry)**2)
    return E, d_true_reco, r_reco, r_reco_vec

a=1
Rm=0.5

n=10
# h_d=(np.sqrt(2*(a/2)**2)/2)/n
h_d=(a/2)/n
h_s=(a/2)/n
diagonal = [(i*h_d,j*h_d) for (i,j) in zip(range(n+1),range(n+1)) ]
side     = [(i*h_s,0) for i in range(n+1) ]
all_cell = [(i*h_s,j*h_d) for i in range(n+1) for j in range(n+1) ]

methods=["2x2","3x3","swisscross"]
conds="_Rm_"+str(Rm)+"_diagonal"
outpath="figs/r_1D/"  #"./"
if not (os.path.isdir(outpath)):
    os.mkdir(outpath)


params_method={}
r_true_list=[]
r_reco_list=[]
d_true_reco_list=[]
E_reco_list=[]

print(diagonal)

# for method in ["2x2"]:
for method in methods:
    r_true_list=[]
    r_reco_list=[]
    d_true_reco_list=[]
    E_reco_list=[]
    for (rx,ry) in diagonal:
        E_cells, cells_centers, cells_borders = get_E_cells(rx,ry,Rm,a)
        E_reco, d_true_reco, r_reco, r_reco_vec = get_params_method(method,
         E_cells, cells_centers, rx, ry)
        r_true=np.sqrt(rx**2+ry**2)
        r_true_list.append(r_true)
        r_reco_list.append(r_reco)
        d_true_reco_list.append(d_true_reco)
        E_reco_list.append(E_reco)
        params_method[method]=(r_true_list.copy(),r_reco_list.copy(),
          d_true_reco_list.copy(),E_reco_list.copy())


for method in methods:
    r_true_list     = params_method[method][0]
    r_reco_list     = params_method[method][1]
    d_true_reco_list    = params_method[method][2]
    E_reco_list     = params_method[method][3]

    plt.plot(r_true_list,E_reco_list,label=method+" diagonal")
    plt.title("E_reco "+method+" Rm="+str(Rm))
    plt.xlabel('r_true')
    plt.ylabel('E_reco/E_true')
    # plt.show()
    plt.savefig(outpath+"E_vs_rtrue_"+method+conds+".png")
    plt.close()

for method in methods:
    r_true_list     = params_method[method][0]
    r_reco_list     = params_method[method][1]
    d_true_reco_list    = params_method[method][2]
    E_reco_list     = params_method[method][3]

    plt.plot(r_reco_list,E_reco_list,label=method+" diagonal")
    plt.title("E_reco "+method+" Rm="+str(Rm))
    plt.xlabel('r_reco')
    plt.ylabel('E_reco/E_true')
    # plt.show()
plt.legend()
plt.savefig(outpath+"E_vs_rreco_"+conds+".png")
plt.close()
