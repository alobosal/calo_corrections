import numpy as np
import matplotlib.pyplot as plt
import pylab as pl
from scipy.integrate import quad, dblquad
import os

# Erec/Etot simulated
def get_E(rx,ry,Rm,x0,x1,y0,y1):
    integ=lambda y, x: 1/(2*np.pi*Rm**2) * np.exp( -(x-rx)**2/(2*Rm**2) -(y-ry)**2/(2*Rm**2))
    return dblquad(integ, x0,x1,lambda x:y0, lambda x:y1)[0]

def get_E_cells(rx,ry,Rm):
    E_cells=np.zeros((3,3))
    cells_borders=np.zeros((3,3),tuple)
    cells_centers=np.zeros((3,3),tuple)
    for i in range(3):
        for j in range(3):
            ix=i-1
            jy=j-1
            cells_borders[i,j]=(-a/2+ix*a,a/2+ix*a,-a/2+jy*a,a/2+jy*a)
            cells_centers[i,j]=(ix*a,jy*a)
            E_cells[i,j]=get_E(rx,ry,Rm,*cells_borders[i,j])
    return E_cells, cells_centers, cells_borders

def E_method(method, E_cells, rx, ry):
    if (method == "2x2"):
        if   (rx<=0 and ry<=0):
            E=E_cells[0,0]+E_cells[1,0]+E_cells[0,1]+E_cells[1,1]
        elif (rx>=0 and ry<=0):
            E=E_cells[1,0]+E_cells[2,0]+E_cells[1,1]+E_cells[2,1]
        elif (rx<=0 and ry>=0):
            E=E_cells[0,1]+E_cells[1,1]+E_cells[0,2]+E_cells[1,2]
        elif (rx>=0 and ry>=0):
            E=E_cells[1,1]+E_cells[2,1]+E_cells[1,2]+E_cells[2,2]
        else:
            E=0
    if (method == "3x3"):
        E = E_cells.sum()
    if (method == "swisscross"):
        E = E_cells[1,0]+E_cells[0,1]+E_cells[1,1]+E_cells[2,1]+E_cells[1,2]
    return E

def get_params_method(method, E_cells, cells_centers, rx, ry):
    if (method == "3x3"):
        cells_indexes= [(i,j) for i in range(3) for j in range(3)]
    if (method == "swisscross"):
        cells_indexes = [(1,0),(0,1),(1,1),(2,1),(1,2)]
    if (method == "2x2"):
        if   (rx<=0 and ry<=0):
            cells_indexes=((0,0),(1,0),(0,1),(1,1))
        elif (rx>=0 and ry<=0):
            cells_indexes=((1,0),(2,0),(1,1),(2,1))
        elif (rx<=0 and ry>=0):
            cells_indexes=((0,1),(1,1),(0,2),(1,2))
        elif (rx>=0 and ry>=0):
            cells_indexes=((1,1),(2,1),(1,2),(2,2))
    E=0
    r_reco_x=0
    r_reco_y=0
    for (i,j) in cells_indexes:
        r_reco_x+=E_cells[i,j]*cells_centers[i,j][0]
        r_reco_y+=E_cells[i,j]*cells_centers[i,j][1]
        E+=E_cells[i,j]
    r_reco_x=r_reco_x/E
    r_reco_y=r_reco_y/E
    r_reco_vec=(r_reco_x,r_reco_y)
    r_reco=np.sqrt((r_reco_x)**2+(r_reco_y)**2)
    d_true_reco=np.sqrt((r_reco_x-rx)**2+(r_reco_y-ry)**2)
    if (method == "2x2" and False):
        if   (rx<=0 and ry<=0):
            new_r0=(-a/2,-a/2)
        elif (rx>=0 and ry<=0):
            new_r0=(a/2,-a/2)
        elif (rx<=0 and ry>=0):
            new_r0=(-a/2,a/2)
        elif (rx>=0 and ry>=0):
            new_r0=(a/2,a/2)
        r_reco=np.sqrt((new_r0[0]-rx)**2+(new_r0[1]-ry)**2)
    return E, d_true_reco, r_reco, r_reco_vec


r=(0,0)

a=1
Rm=1

x,y = pl.ogrid[-a/2:a/2:a/50, -a/2:a/2:a/50]

# 2D plots
def E_2x2(rx,ry):
    method="2x2"
    E_cells, cells_centers, cells_borders = get_E_cells(rx,ry,Rm)
    return E_method(method, E_cells, rx, ry)

def E_3x3(rx,ry):
    method="3x3"
    E_cells, cells_centers, cells_borders = get_E_cells(rx,ry,Rm)
    return E_method(method, E_cells, rx, ry)

def E_swisscross(rx,ry):
    method="swisscross"
    E_cells, cells_centers, cells_borders = get_E_cells(rx,ry,Rm)
    return E_method(method, E_cells, rx, ry)

def r_2x2(rx,ry):
    method="2x2"
    E_cells, cells_centers, cells_borders = get_E_cells(rx,ry,Rm)
    return get_params_method(method, E_cells, cells_centers, rx, ry)[2]

def r_3x3(rx,ry):
    method="3x3"
    E_cells, cells_centers, cells_borders = get_E_cells(rx,ry,Rm)
    return get_params_method(method, E_cells, cells_centers, rx, ry)[2]

def r_swisscross(rx,ry):
    method="swisscross"
    E_cells, cells_centers, cells_borders = get_E_cells(rx,ry,Rm)
    return get_params_method(method, E_cells, cells_centers, rx, ry)[2]

def d_2x2(rx,ry):
    method="2x2"
    E_cells, cells_centers, cells_borders = get_E_cells(rx,ry,Rm)
    return get_params_method(method, E_cells, cells_centers, rx, ry)[1]

def d_3x3(rx,ry):
    method="3x3"
    E_cells, cells_centers, cells_borders = get_E_cells(rx,ry,Rm)
    return get_params_method(method, E_cells, cells_centers, rx, ry)[1]

def d_swisscross(rx,ry):
    method="swisscross"
    E_cells, cells_centers, cells_borders = get_E_cells(rx,ry,Rm)
    return get_params_method(method, E_cells, cells_centers, rx, ry)[1]


methods=["2x2","3x3","swisscross"]
energy_funcs={"2x2":E_2x2,"3x3":E_3x3,"swisscross":E_swisscross}
distance_funcs={"2x2":d_2x2,"3x3":d_3x3,"swisscross":d_swisscross}
r_reco_funcs={"2x2":r_2x2,"3x3":r_3x3,"swisscross":r_swisscross}

conds="_Rm"+str(Rm)+""
outpath="figs/r_1/"  #"./"
if not (os.path.isdir(outpath)):
    os.mkdir(outpath)

# for method in ["2x2"]:
for method in methods:
    E_vec=np.vectorize(energy_funcs[method])
    E_reco_2D=E_vec(x,y)
    plt.imshow(E_reco_2D.transpose(),origin="lower")
    plt.colorbar()
    plt.title("E_reco "+method+ "Rm="+str(Rm))
    # plt.show()
    plt.savefig(outpath+"E_"+method+conds+".png")
    plt.close()

    d_vec=np.vectorize(distance_funcs[method])
    ds_2D=d_vec(x,y)
    plt.imshow(ds_2D.transpose(),origin="lower")
    plt.colorbar()
    plt.title("d_reco_true "+method+ "Rm="+str(Rm))
    # plt.show()
    plt.savefig(outpath+"d_reco_true_"+method+conds+".png")
    plt.close()

    r_vec=np.vectorize(r_reco_funcs[method])
    rs_2D=r_vec(x,y)
    plt.imshow(rs_2D.transpose(),origin="lower")
    plt.colorbar()
    plt.title("r_reco "+method+" Rm="+str(Rm))
    # plt.show()
    plt.savefig(outpath+"r_reco"+method+conds+".png")
    plt.close()
